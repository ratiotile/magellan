requirements:
cybw
pillow

Use Python 3.4 built with msvc2013.
NB: Pillow needs to be built from source or it will crash. Use version < 3, as version 3 will fail to build without jpeg external library.
