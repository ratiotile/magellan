from cybw import TilePosition, Position, Colors, NullPointerException
import cybw


bw = cybw.Broodwar

def forEachScreenTile(f):
    screen_x, screen_y = TilePosition(cybw.Broodwar.getScreenPosition()).getXY()
    #print("screenpos {},{}".format(screen_x,screen_y))
    screen_width = 20
    screen_height = 15
    for x in range(screen_x, screen_x + screen_width):
        for y in range(screen_y, screen_y + screen_height):
            f(TilePosition(x, y))

def drawRegionBorderSE(tile):
    try:
        r1 = bw.getRegionAt(Position(tile))
    except NullPointerException:
        return
        #print("could not get region for {} - {}".format(tile, Position(tile)))

    pos = Position(tile)
    if tile.x+1 < bw.mapWidth():
        te = TilePosition(tile.x+1, tile.y)
        pe = Position(te)
        try: # necessary for the weird map shape around south edge for gui
            r2 = bw.getRegionAt(pe)
            if r1 != r2:
                bw.drawLineMap(pe.x, pos.y, pe.x, pe.y+32, Colors.Red)
        except NullPointerException:
            pass
    if tile.y+1 < bw.mapHeight():
        ts = TilePosition(tile.x, tile.y+1)
        ps = Position(ts)
        try:
            r2 = bw.getRegionAt(ps)
            if r1 != r2:
                bw.drawLineMap(ps.x, ps.y, ps.x+32, ps.y, Colors.Red)
        except NullPointerException:
            pass

def drawRegionIds():
    for region in bw.getAllRegions():
        pos = region.getCenter()
        if isOnScreen(pos):
            bw.drawTextMap(pos, str(region.getID()))


def isOnScreen(pos):
    screenpos = bw.getScreenPosition()
    if pos.x < screenpos.x or pos.y < screenpos.y:
        return False
    elif pos.x > screenpos.x + 640 or pos.y > screenpos.y + 480:
        return False
    return True
