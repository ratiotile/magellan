import sys
import os
import configparser
from dotmap import DotMap

_config = None

def initialize():
    global _config
    path = os.path.dirname(os.path.relpath(__file__))
    filepath = os.path.join(path, "default_settings.ini")
    parser = configparser.ConfigParser()
    parser.read(filepath)
    config  = {s:dict(parser.items(s)) for s in parser.sections()}
    _config = DotMap(config)
    for key, val in _config.items():
        sys.modules[__name__].__dict__[key] = val
