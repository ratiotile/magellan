import pickle
import gzip
import os

class MapFile:

    def __init__(self, filename):
        self.filename = filename

    def save(self, data):
        ensureDirectoryExists(self.filename)
        output = gzip.GzipFile(self.filename, 'wb')
        pickle.dump(data, output, -1)
        output.close()

    def load(self):
        unzipped_file = gzip.GzipFile(self.filename, 'rb')
        data = pickle.load(unzipped_file)
        unzipped_file.close()
        return data


def ensureDirectoryExists(path):
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path), exist_ok=True)
