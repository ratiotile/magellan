from setuptools import setup


setup(
    name='Magellan',
    version='0.0.1',
    description='Starcraft map analysis',
    url='https://bitbucket.org/ratiotile/magellan',
    license='BSD',
    classifiers=[
      'Development Status :: 3 - Alpha',
      'Intended Audience :: Developers',
      'Programming Language :: Python :: 3.4',
    ],
    keywords='bwapi map analysis bwta',
    packages=['magellan']
)
