﻿import cybw
import logging
from time import sleep
from magellan.TerrainAnalyzer import TerrainAnalyzer
from magellan.DebugDraw import (forEachScreenTile, drawRegionBorderSE,
    drawRegionIds)

client = cybw.BWAPIClient
Broodwar = cybw.Broodwar

im = None
draw = None

TA = None

logger = logging.getLogger("magellan")

class FrameFilter(logging.Filter):
    def __init__(self, bw):
        super().__init__()
        self.bw = bw
    def filter(self, record):
        record.frame = self.bw.getFrameCount()
        return True

# necessary on Windows to protect entry point
if __name__ == "__main__":
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler("magellan.log", mode='w')
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    fh.setLevel(logging.DEBUG)
    filter = FrameFilter(Broodwar)
    formatter = logging.Formatter(
        '%(frame)s|%(asctime)s.%(msecs).03d - %(name)s - %(levelname)s - %('
        'message)s',
        '%H:%M:%S')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    fh.addFilter(filter)
    ch.addFilter(filter)
    logger.addHandler(fh)
    logger.addHandler(ch)

def reconnect():
    while not client.connect():
        sleep(0.5)

print("Connecting...")
reconnect()
while True:
    print("waiting to enter match")
    while not Broodwar.isInGame():
        client.update()
        if not client.isConnected():
            print("Reconnecting...")
            reconnect()
    print("starting match!")

    Broodwar.sendText("black sheep wall")
    print(Broodwar.mapFileName())
    TA = TerrainAnalyzer(Broodwar)
    TA.analyze()
    Broodwar.enableFlag(cybw.Flag.UserInput)
    #Broodwar.leaveGame()

    while Broodwar.isInGame():
        client.update()
        forEachScreenTile(drawRegionBorderSE)
        drawRegionIds()
        #print("screenpos {}".format(Broodwar.getScreenPosition()))
